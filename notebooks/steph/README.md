# Steph's ECE445 Notebook
 - [Development](#development) 
 - [Parts List](#parts-list)
 - [Debugging](#debugging)

# Development
 ## 2/7/2022
 This week we created a preliminary proposal for TA meeting and began thinking about parts and design.
 ## 2/14/2022
 We began creating a parts list and doing research on other PCB designs. We are looking to use the datasheets for IMU boards available for purchase and integrating this into our PCB design. We looked into microcontrollers and decided preliminarily to use the WROOM as its communication protocols align with our design. We were informed that we are unable to use members of the skating team to test our project, so we redefined the moves that we are going to be testing for ease of access to testing data.
 ## 2/21/2022
 This week we had our first design meeting with a professor for the course. We went though our high level requirements and were told to quantify them. Our block diagram needs to expand to account for multiple IMU nodes but good reviews otherwise. We also need to add to OpenCV accuracy calculations to our tolerance analysis. In terms of software we found good material on gitHub and some other codebase platforms. We began researching cameras as we found that some of the material for modeling the human in our software requires a depth camera to extrapolate accurate data.
 ## 2/28/2022
 This week we took feedback from the professors and TAs and have worked on our PCB. We defined a parts list and are attempting to purchase in the near future in order to ensure that we are able to get parts on time. Lionel is going to reach back out to GermanyMan to obtain feedback and software to continue our development of our program. We also obtained 3D printer filament and are working to create a CAD file for the cases for the wearables to match the size of the PCB that we will be mounting inside. We have access to GoPro cameras and are looking to define which one we will be using. The professor that we got our design reviewed by brought up the point that if we are able to define motion through still images we can also go that route if video footage is too buggy to work with in our program.
## 3/7/2022
This week we worked largely on the PCB and parts ordering. I clarified my description of the design in order to help Ethan's progress towards completing the PCB design, and also had some suggestions such as adding ports for microcontroller programming. We also finalized our parts list for ordering.
## 3/14/2022
Spring Break!
## 3/21/2022
This week we ran into a bit of an issue with our parts order. The IMU we had initially selected was no longer available and finding a replacement has proved to be more difficult than we anticipated. We are also beginning to decide the breakdown of what software to work on between Lionel and myself.
## 3/28/2022
This week we have made progress towards our replacement IMU and are now ready to order that part. We have also decided which open source software libraries we will be pulling from to create the simulation of the skater. We will be using OpenCV for object tracking, OpenPose for modeling, and OpenSim for simulation. I have decided to begin the OpenCV portion of the software while Lionel takes on OpenPose. We also have to implement sensor fusion with our new IMU so we will need to find libraries for that as well.

# Parts List
 - BNO055 Sensor - x1 for each sensor unit
 - microcontroller (ESP 32 WROOM (?)) - for main central unit
 - ribbon cables
 - 2S LiPo Battery (7.4 V)
 - SD breakout board (micro vs. normal)
 - camera (GoPro, 30 FPS -or- Intel Depth Camera)
 - RTC (Real-Time Clock Chip, i2c connection)
 - resistors
 - capacitors
 - voltage regulator
 - Thermoplastic Polyurethane (TPU) -> 3D printing casings
 - mounting screws
 - LEDs

# Debugging
Issue 0:
