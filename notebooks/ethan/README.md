# Ethan's ECE 445 Notebook

### Development

- 2/7

###### Introduction to Project

### Objectives:
- Introduction to team
- Understand goals and objectives of project
- Get acquainted with new teammates

After being invited to this project by Steph and Lionel, I was caught up on the concepts and ideas that went into the basic structure of this project. The project is a Digital Coach for Figure Skating, which holds the purpose of being a cost-effective alternative to current methods. Essentially, the user would strap on a set of IMU sensors while being video-recorded with a camera, to track their position and movement while they skate. Their movement would then be compared to an ideal model of certain moves, receiving feedback on what to improve or change in their form, posture, and movement. We also set up and updated our Github page with our proposal and notebooks.

- 2/9
 
###### Discussing Design

### Objectives:
- Begin talking physical/hardware design
- Determine suitable design possibilities
- Understand possible points of weakness and solutions
 
For this meeting, we met up to go over possible designs for an IMU sensor. This pertains to many aspects, such as casing material, necessary interface protocols, and a realistic idea of what our design would look like in action. One of the main components is the BNO055 sensor, which contains an accelerometer, gyroscope, magnetic sensor, and microcontroller, which would be very handy in acquiring positional and rotational data for our project. Our IMU sensors would each be encased in some sort of fall-resistant material, and then each one linked by ribbon cable to the main central unit, which would most likely be held around the torso area. We also went over the basic components for our block diagram. As a basis to our actual project, we were tidying up and editing our proposal to submit to the ECE 445 web board.

- 2/14

###### Design Document

### Objectives:


After submitting our proposal, our next objective was to compose our design by specifying parts and understanding how we would integrate each part into the whole. Though not official, we are currently looking at the ESP 32 WROOM module to be our microcontroller. To see what else was available, we also took a brief look to search the ECE parts available to us and see if there was anything that we else we might need to build the electrical/physical model of our design. I also took the additional time to look into the schematics and design of certain parts to see how compatible they would be, as well as how easy it would be to interface different elements. We came up with a parts list, which is subject to change as we continue to conceptualilze the electrical design of our model, but currently contains most of the main basic elements.

### Parts List

- BNO055 Sensor - x1 for each sensor unit, x6 total currently)
- microcontroller (ESP 32 WROOM) - for main central unit
- ribbon cables
- 2S LiPo Battery (7.4 V)
- MicroSD breakout board
- camera (GoPro, 30 FPS)
- RTC (DS1307 - Real-Time Clock Chip, i2c connection)
- resistors
- capacitors
- voltage regulator (LM1117)
- Thermoplastic Polyurethane (TPU) -> 3D printing casings
- mounting screws
- LEDs

2/16

###### Putting the Parts Together

We looked over the parts, their availability, and costs, to get a general sense of what we were going to use to construct our project. This included looking at the parts shop and list for available parts to see what we find already available instead of waiting for shipping and extra costs. I briefly started designing the electrical circuit schematic, looking at how each individual chip and processor unit is powered and what interfaces it uses to identify the necessary buses. We have to design 2 different sets of schematics: one for the IMU sensors that the user will wear around their body, and the main central unit that collects all the data from the individual sensors. During the meeting, we also discussed how to improve the camera respresentation in the project so that we were optimizing and utilizing both the camera and the IMUs for complete tracking.

2/21

###### Design Document Check

Today, we had our design document check with Prof. Schuh. After presenting our project idea and designs with him, we receieved some feedback to improve our design document, as well as our sophistication and specifications of the different aspects of our project. This included quantifying some of our high level requirements, reevaluting one of our verification tables, updating our block diagram to be more accurate with our physical design, and completing a tolerance analysis based on the tracking aspect of our project. Overall, we found the Q&A session and the review to be very helpful as it guided us to focus in more on individual portions of our project and how to make our purpose and goals clear.

2/23

###### Finalizing Design Document

At this meeting, we discussed the different points of revision that were mentioned at our design document check. We mainly spent this time going over those points and setting either reasonable changes or quantifiable tolerances that define the accuracy of our scope. We went back and redid some of the R&V tables to make them more realistic in terms of each subsystem's performance. I was able to define a tolerance analysis based on tracking, by analyzing the camera's vision, tolerance rates of the sensor. This tolerance analysis is basically a limit test for the accuracy of camera tracking, and how far off it can be in terms of pixels. It takes into account the different viewing angles of the camera, and the minimum to maximum distance of the skater from the camera for different cases, providing a range of pixels for how well the software should be able to track the IMU nodes in respect to the skater's distance from the camera.

2/28

###### Design Review

The design review went mostly well. The professor had a few question based on the implementation of both our IMU and camera data aggregation. As the camera should be able to do most of the work, using the IMU sensor data is optimal if we were to implement other moves besides the Bielmann into our program (which would be a further design implementation after our set of data for our first move is calculated). In terms of other progression, the PCB had to be redesigned to fix the control unit. The IMU sensor also had to be remodeled as the BNO055 is out of stock, so it had to be switched to the BHI260AB. This is unfortunate because we changed from a 9-DOF sensor to a 6-DOF sensor, losing the magnetometer measurements that the BNO055 was able to perform. Realistically, we should still be able to use it in our data measurements and implement it into our design. Luckily, the BHI260AB sensor still uses data fusion, so we won't have to manually aggregate our data if there is a spike or outlier.

3/2

###### Overview

This meeting was pretty basic. We went over the feedback that we heard during our presentation for our final document review, and revised and discussed all points so that the finalized form would be clear and specific in letting other people understand our project and its goals.

3/9

###### Checkpoints

This meeting was basically just another brief session to understand our goals and current progress. We each filled out the team progress reports, and then discussed what our plan was for when we got back from break. Expecting to start hardware construction within the week of getting back.

3/21

###### Parts Confirmation

This was just a meeting between Lionel and I, where we confirmed the parts necessary to order for our hardware design, as well as change other components based on the availablity and delivery of the currents ones in our schematic. This included changing voltage regulators, finding the right component designs for soldering, and putting in our parts order. We spent some time going over several models of different components to seee what we could use for our hardware. Even though our PCB technically designates specific footprints, several models look similar and we were trying to be cost-effective as well as find suitable parts that still met our needs since the PCB will be hidden from view once fully assembled and connected.

3/23

###### Setback Again

During this meeting, we met up at ECEB to pick up our PCB and any electrical parts we could use from the Electronics Service Shop. We were hit by another major setback as we had to change the IMU sensor for the 2nd time: the distributor cancelled our order after it was put in and labeled our second choice of sensors as "Out of Stock". We plan to redesign the IMU sensor once again from using the BHI260AB to a new one, which is unfortunate as the other options are older models or do not have data fusion. This means we'll have to make a few adjustments in the code to account for the data inaccuracies. It is very frustrating, but we are trying to keep moving forward. Since we need to change the control unit to account for a different IMU sensor later on, we can do some small voltage and continuity tests on our board, but it would be best not to test too much as our resources for the hardware can be costly.

3/28

###### New IMU Sensor

In this meeting, we determined our new sensor as the ISM330DHCXTR, which seems to be perfectly fine in terms of stock and availability. It unforunately does not integrate data fusion, so we will have to fix our own data as it runs, but it is only a minor issue. Steph and Lionel also talked about the code, and how exactly we will use and aggregate our data. This led down to which filters to use to track the IMU nodes on camera, as well as how the IMU sensor will decipher its output data in combination with the camera data. This is another setback, and I will be have to redesign the PCB to account for the change of the IMU sensor. This means slightly changing the control unit schematic to have the necessary outputs for the ISM330DHCXTR, while completely scrapping the old IMU PCB unit and starting from scratch.

3/30

4/4

4/6

4/11

4/13

4/18

4/20

4/22

Today was our Mock Demo. While we got a large amount of work done in the past week, there were still many things to continue working on and improve upon. At this point, we were able to describe what work we had done and how far we had gotten overall, but we were not able to show anything remotely similar to what we were hoping for as a demonstration. I was able to let Zhicong take a look at the complted control unit, along with one of the IMU units. While they were assembled individually, they had not been wired together yet to run data collection tests. At this point, I have been able to perform simple power tests on both units. Stephanie 

4/24

4/26

4/27

4/28

Final Demo

4/29

4/30

5/1

5/2

Final Presentation
