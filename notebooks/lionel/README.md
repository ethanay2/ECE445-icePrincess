# Lionel's ECE445 Notebook
 - [2022-02-03](#2022-02-03) 
 - [2022-02-07](#2022-02-07) 
 - [2022-02-08](#2022-02-08)
 - [2022-02-09](#2022-02-09) 
 - [2022-02-10](#2022-02-10)  
 - [2022-02-14](#2022-02-14)   
 - [2022-02-16](#2022-02-16) 
 - [2022-02-20](#2022-02-20) 
 - [2022-02-21](#2022-02-21) 
 - [2022-02-23](#2022-02-23) 
 - [2022-02-24](#2022-02-24) 
 - [2022-02-28](#2022-02-28) 
 - [2022-03-03](#2022-03-03) 
 - [2022-03-07](#2022-03-07) 
 - [2022-03-09](#2022-03-09) 
 - [2022-03-21](#2022-03-21) 
 - [2022-03-23](#2022-03-23) 
 - [2022-03-28](#2022-03-28) 
 - [2022-03-30](#2022-03-30) 
 - [2022-04-03](#2022-04-03) 
 - [2022-04-04](#2022-04-04) 
 - [2022-04-05](#2022-04-05)
 - [2022-04-11](#2022-04-11)
 - [2022-04-13](#2022-04-13)
 - [2022-04-20](#2022-04-20)
 - [2022-04-21](#2022-04-21)
 - [2022-04-22](#2022-04-22)
 - [2022-04-24](#2022-04-24)
 - [2022-04-26](#2022-04-26)
 - [2022-04-27](#2022-04-27)
 - [2022-04-28](#2022-04-28)
 - [2022-04-29](#2022-04-29)
 - [2022-04-30](#2022-04-30)
 - [2022-05-01](#2022-05-01)
 - [2022-05-02](#2022-05-02)

## 2022-02-03
We have finalized our group by adding a third member and are setting up an initial meeting.

## 2022-02-07
First meeting between the three of us. We have set up twice-weekly meeting times (MW afternoons) so that we can stay up to date with each others' progress and have time to collaborate.

## 2022-02-08
First meeting with our TA. Mentioned that project would be a difficult undertaking and together we discussed briefly areas which we could simplify the project in order to make sure it gets done on time.

## 2022-02-09

## 2022-02-10
summary of what me and TA (David?) talked about:
- best interface protocol is kind of just dependent on what electronics we pick that can satisfy our requirements, so we should do a little research and pick an IMU and microcontroller for now that have compatible communication protocols.
- he mentioned that using IMUs will likely be a lot more difficult than we anticipate. There's apparently a phenomenon called "drifting" where an IMU left alone, in his example for ~5 minutes, will start rotating its own orientation reading without actually moving. He also drew me a diagram where an IMU is tilted w.r.t. the ground and therefore has a gravity vector pointing out of it. Not entirely sure how this was relevant but he mentioned needing to calculate things like the angle between gravity vector and the board, which is very difficult to do. Gyro data can help make up for stuff like "drift" but doing sensor fusion calculations is also very difficult. So one IMU he recommended looking at is the BNO055 which apparently can do some of this math internally, but doesn't resolve the issue perfectly. I also showed him the IMU that Alan Tokarsky sent and he said he probably wouldn't want to use that one specifically. He also mentioned that it might just be a good idea to manufacture the IMUs ourselves by copying the data sheet schematics, but idk if I'm entirely convinced about that.
- we also talked about how to time the IMU and camera data to sync them up. I brought up the RTC chip which he agreed was one way to do it. from there we kind of identified either the timing issue or the motion tracking system accuracy as an area to focus on for the tolerance analysis section of the proposal. I'm not sure how we can determine acceptable failure tolerances for these things without actually testing a finished product and seeing what works, but I guess that's something we need to estimate now and are somehow required to prove through mathematical analysis or simulation. (more in next msg)
- for the ethics section, we probably don't have any ethical concerns, but it's still good to include a blurb about not having any and our reasoning behind that in order to demonstrate that we've considered ethical implications.

## 2022-02-14

## 2022-02-16

## 2022-02-20

## 2022-02-21
Attended design document check with Prof. Schuh, did not get roasted nearly as badly as expected. I take it as a sign that we are making more progress than I had initially anticipated compared to other groups, and we received some very solid feedback that we will try to incorporate by the DD deadline on Thursday.

## 2022-02-23
Met with the team to discuss implementation of changes mentioned in the design document check. My sections of focus would be fleshing out the R&V tables, revising the subsystem descriptions, and updating the safety & ethics section. As a team, we also reviewed our high-level requirements and made some slight modifications as requested by Prof. Schuh.

## 2022-02-24
We finished the design document and submitted it before the deadline. I completed all of my assignments (as mentioned in the previous day's post) and formatted our final submission from Google Doc into the word document. Steph worked on the visuals as well as cost & scheduling, while Ethan worked on the schematics and the tolerance analysis. 

## 2022-02-28
Attended our design review with Prof. Song. He was very interested in the product as he could envision using it himself for golf and saw a lot of potential in terms of marketability for a lower cost product compared to what's on the market now. I think the general feeling was that our project is good, but potentially may be too overambitious for the time and design limitations we have to complete it. He suggested coming up with a backup solution in which we analyze the camera feed frame by frame, so that we are working with still images instead of a video. This could simplify the project enough in case we encounter difficulties making the video feed work.

## 2022-03-03
Team meeting to discuss the feedback received at the board review. We also went over what our objectives and deliverables would be for the next week leading up to spring break.

## 2022-03-07
Another short team meeting talking about similar things as last week. Checked in on Ethan's progress towards completing the PCBs.

## 2022-03-09

## 2022-03-21
Met with Ethan and found replacement voltage regulators, also determined which battery to buy. Ordered all parts.

## 2022-03-23
Team meeting to try and find a replacement for the BHI260AB IMU sensor, which went out of stock the day after we submitted the purchase order for it. Looked through practically all of the Bosch family of IMUs and everything is out of stock or discontinued. Sent in a quote request for the BMI055 on a long shot, but will look for alternatives. We started looking at other brands of sensors as well, but there doesn't seem to be anything else that has sensor fusion integrated. Top prospect is the ISM330DHCXTR by STMicroelectronics (https://estore.st.com/en/ism330dhcxtr-cpn.html).

## 2022-03-28
No response on the quote request yet, so we decided to just go with the ISM sensor for Ethan to be able to re-design the IMU PCB in time for the second round of orders. We started looking into some sensor fusion algorithms that we can apply using Python.

## 2022-03-30
Wrote individual report detailing my work on the project so far and what I still plan to do.

## 2022-04-03
Starting working on OpenPose setup on my laptop. In order to modify the existing code for our project, I would need to compile and run the OpenPose code from Source. This requires me to delete Anaconda (interferes with the program somehow) and download a slew of other prerequisite software. I attenmpted this, but I am having issues with things like cudNN because the installation guides do not match up with my system.

## 2022-04-04
Pivoted to using the Windows portable demo of OpenPose which requires significantly less setup work. I did run into an issue with getting OpenCV set up properly, but I found documentation addressing this issue and will try to implement their recommended fix. If it doesn't work, I might have to consider pivoting to a Linux system which seems to have more support and less hassle for using OpenPose. However this would raise new concerns about how to acquire a Linux system and the impact this would have on being able to interface with other aspects of the project (both hardware and software) as well as the theoretical commercial feasibilty of requiring our project to be deployed on Ubuntu, but this is less of a concern that being able to get the project working at all.

## 2022-04-05
Brought up two ideas to Zhicong, using a Linux VM and creating a separate Windows user to try and bypass the issues with software being pre-downloaded. Need to find a way to get a clean slate. Linux VM would allow for better support as there are more detailed and better tailored instructions for that OS. 

## 2022-04-11
Tried Linux VM and realized separate Windows user was a dumb idea since multiple users still seem to share the same program files.

## 2022-04-13
Talked to Zhicong again and he suggested using WSL, built-in Linux environment for Windows OS. Looked into WSL differences to VM and how it could potentially affect the project, but the discussion was at a technical level above my experience. Tried installing and ran into issues surrounding this error, "virtual hard disk files must be uncompressed and unencrypted and must not be sparse." May be pivoting to using Xbox Kinect camera and some compatible libraries to do skeleton processing instead. 

## 2022-04-20
Team meeting at the ESPL, downloaded Processing IDE and installed j4k library into proper location. 

## 2022-04-21


## 2022-04-22
Mock demo

## 2022-04-24


## 2022-04-26


## 2022-04-27


## 2022-04-28
Demo and prepared for mock presentation.

## 2022-04-29
Mock presentation

## 2022-04-30
Attempted to resolder voltage regulator, ended up shorting the entire board where nothing powers up anymore. Set up ESP32 dev board to attempt to program IMUs individually, encountered compile errors that were not resolved.

## 2022-05-01
Took photos and videos for presentation, finished slides

## 2022-05-02
Project presentation prep and delivery


